package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

public class ConstraintEq extends Constraint {

	ConstraintEq(BufferedReader in) throws Exception {
		super(in);
	}

	@Override
	public boolean violation(Assignment a) {

		for(String var : this.getVars()){
			if(!(a.getVars().contains(var)))
				return false;
		}
                boolean res = true;
		for (int i=0; i < this.getVars().size() - 1;i++){
                        for (int j=i+1;j<getVars().size();j++){
                            res=res && (a.get(this.getVars().get(i)).equals(a.get(this.getVars().get(j))));
                        }
                    }
		return !res;
	}

	@Override
	public String toString() {
		return "\n\t Eq "+ super.toString();
	}
}