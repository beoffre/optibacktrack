/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csp_etud;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.time.Instant;

/**
 *
 * @author felix.yriarte@etu.umontpellier.fr
 */
public class Application {
                                // test git
    public static void main(String[] args) throws IOException {
            try {
                if (args.length==0){System.exit(0);}
                String fileName = "reseaux\\ExperimentFiles\\" + args[0] ;
            //Print monOutput = new FilterOutputStream("out.txt");
            //System.setOut(new PrintStream("out.txt"));
            System.setProperty("line.separator","\n");
            Network myNetwork;
            //System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath() + "/" + fileName);
            BufferedReader readFile = new BufferedReader(new FileReader (fileName));
            myNetwork = new Network(readFile);
            readFile.close();
            System.out.println(myNetwork);
            CSP myCSP = new CSP( myNetwork );
            //List<Assignment> resL ;            
            Assignment res;
            Long timeDeb = System.currentTimeMillis();
            res=myCSP.searchSolution();

            System.out.println("voici sol : "+res+"\n time : "+ (System.currentTimeMillis()-timeDeb)+"\n explo de noeuds : "+myCSP.cptr);
            }
            catch(Exception e){System.out.println("Erreur lors du chargement : " + e.getMessage());}
            
           
	}
}
