/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
/**
 *
 * @author felix.yriarte@etu.umontpellier.fr
 */
public class ConstraintExp extends Constraint {
    
    private String expression; 
    private static ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");

    private static void enginePut(ScriptEngine e, String var, Object value) {
        try {
            e.put(var, Integer.parseInt(value.toString()));
        } catch (NumberFormatException nfe) {
            try {
                e.put(var, Float.parseFloat(value.toString()));
            } catch (NumberFormatException nfe2) {
                if (value.toString().equalsIgnoreCase("false") || value.toString().equalsIgnoreCase("true")) {
                    e.put(var, Boolean.parseBoolean(value.toString()));
                } else {
                    e.put(var, value);
                }
            }
        }
    }

    
    
    ConstraintExp(BufferedReader in) throws Exception {
		super(in);
                this.expression=in.readLine();
	}
    
    @Override
    public boolean violation (Assignment a){
        for(String maVar : this.getVars()){
            if (!(a.getVars().contains(maVar))){return false ;}
        }
        for (String maVar : this.getVars()){
            enginePut(this.engine, maVar, a.get(maVar));
        }
        boolean res=false;
        try{res =((Boolean)engine.eval(this.expression));}
        catch(ScriptException e){System.err.println("prob eval: " + this.expression);}
        return !res;
    }
    @Override
	public String toString() {
		return "\n\t Exp "+ super.toString();
	}
}
