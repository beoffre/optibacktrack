/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csp_etud;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;
import java.util.List;
import java.time.Instant;
import java.io.PrintStream;
/**
 *
 * @author felix.yriarte@etu.umontpellier.fr
 */
public class demo {
    
    public static void main(String[] args) throws IOException {
            try {
                System.setProperty("line.separator","\n");
                FileOutputStream out = new FileOutputStream("touslesresultat.txt",true);
                for (int nbrFiles = 0 ; nbrFiles<130;nbrFiles++){
                    for (int nbrEssais =0 ; nbrEssais<5 ; nbrEssais++){
                        String fileName = "reseaux\\ExperimentFiles\\outfic";
                        if (nbrFiles<10){fileName = fileName + "0";}
                        fileName=fileName+nbrFiles+".txt";
                        Network myNetwork;
                        BufferedReader readFile = new BufferedReader(new FileReader (fileName));
                        myNetwork = new Network(readFile);
                        readFile.close();
                        CSP myCSP = new CSP( myNetwork );
                        Assignment res;
                        Long timeDeb = System.currentTimeMillis();
                        res=myCSP.searchSolution();
                        String ligne = nbrFiles+"_"+nbrEssais+";"+(System.currentTimeMillis()-timeDeb)+";"+myCSP.cptr+";"+(!(res==null))+";";
                        
                        out.write(ligne.getBytes());
                        
                    }
                    
                    out.write("\n".getBytes());
                    
                }
                out.close();  
            }  
            catch(Exception e){System.out.println("Erreur lors du chargement : " + e.getMessage());} 
	}

}





/**
 *
 * @author felix.yriarte@etu.umontpellier.fr
 */
    
    